package com.onepay.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.onepay.model.WalletOpsTxnDTO;

@Repository
public interface WalletOpsTxnRepositor extends CrudRepository<WalletOpsTxnDTO, Serializable>  {
	
	 @Query("FROM WalletOpsTxnDTO w  where w.created_on between ?1 and ?2")
     ArrayList<WalletOpsTxnDTO> findBeetweenDates(Date fromdate, Date todate);

	 @Modifying(clearAutomatically = true)
	 @Transactional
	 @Query("UPDATE WalletOpsTxnDTO w SET w.status = ?1 WHERE w.wallet_id = ?2")
	 int updateRejectStatusWallet(String string, String walletId);
	 
 
}
