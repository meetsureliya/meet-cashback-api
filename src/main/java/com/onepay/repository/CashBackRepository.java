
package com.onepay.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.onepay.model.CashbackFileDTO;

@Repository
public interface CashBackRepository extends CrudRepository<CashbackFileDTO, Serializable> {

	@Query("FROM CashbackFileDTO c  where c.upload_date_time between ?1 and ?2")
	ArrayList<CashbackFileDTO> findBeetweenDates(Date fromdate, Date todate);

    @Modifying
	@Transactional
	@Query(value = "INSERT INTO cashback_file(file_name, uploaded_file_name,process_datetime,status,upload_date_time,uploaded_by,modified_on,total_count,valid_count,invalid_count,total_amount) VALUES (?1, ?2,?3,?4,?5,?6,?7,?8,?9,?10,?11)", nativeQuery = true)
	void insertCashBack(String file_name, String uploaded_file_name, Date process_datetime, String status,
			Date upload_date_time, String uploaded_by, Date modified_on, String total_count, String valid_count,
			String invalid_count, String total_amount);
    
    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE CashbackFileDTO c SET c.status = ?1 WHERE c.uploaded_file_name = ?2")
    int updateRejectStatus(String status,String file_name );
	
	@Query("FROM CashbackFileDTO c  where c.uploaded_file_name = ?1")
	ArrayList<CashbackFileDTO> findByOrgFileName(String uploaded_file_name);
	
	@Query("FROM CashbackFileDTO c  where c.file_name = ?1")
	ArrayList<CashbackFileDTO> findByFileName(String file_name);


}
