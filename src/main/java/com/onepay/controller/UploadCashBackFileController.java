package com.onepay.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.onepay.exception.FileStorageException;
import com.onepay.model.CashbackFileDTO;
import com.onepay.Response.Response;
import com.onepay.service.CommonMethodService;
import com.onepay.service.UploadCashbackFileService;
import com.onepay.sftp.SftpConfig;

@RestController
public class UploadCashBackFileController {

	@Autowired
	private UploadCashbackFileService fileStorageService;
	
	@Autowired
	private CommonMethodService commonMethodService;
	
	@Autowired
	private SftpConfig sftpConfig;

	
	@PostMapping("/uploadCsvFile")
	public Response uploadCsvFile(@RequestParam("file") MultipartFile file) {
		String orgFileName = file.getOriginalFilename();

		if (orgFileName.lastIndexOf(".") != -1 && orgFileName.lastIndexOf(".") != 0 && !orgFileName.contains(".."))
			if (!orgFileName.substring(orgFileName.lastIndexOf(".") + 1).equalsIgnoreCase("csv")) {
				return new Response("Fail ! Only csv Format requiered", orgFileName, file.getContentType(),
						file.getSize());
			} else {

				try {
					String fileName = fileStorageService.storeFile(file);
					//String status = sftpConfig.connectUploadSftpFiles(file);
					//if(status.equalsIgnoreCase("Success")){}
					
					ArrayList<CashbackFileDTO> list = fileStorageService.readCsvFile(file, null, "USER",fileName);
					String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
							.path(fileName).toUriString();
					return new Response("File has been uploaded successfully", fileName, file.getContentType(), file.getSize());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return new Response("failure", orgFileName, file.getContentType(), file.getSize());

				}

			}
		return null;
	}
	
	
	
	@PostMapping("/uploadCashbackFile")
	public Response uploadCsvFileAdministrator(@RequestParam("file") MultipartFile file,@RequestParam("date") String fileDate) throws ParseException {
		String orgFileName = file.getOriginalFilename();
		String fileName = null;
		
		if (isThisDateValid(fileDate, "yyyy-MM-dd HH:mm:ss") != true) {
			return new Response("Fail ! fileDate dateFormat is not Correct, Kindly Use yyyy-MM-dd HH:mm:ss Format", orgFileName, file.getContentType(),
					file.getSize());
		}
		
		Date date=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fileDate);
		
		if(commonMethodService.findByOrgFileName(orgFileName) == true ) {
			return new Response("Fail ! File Already exist!", orgFileName, file.getContentType(),
					file.getSize());
		}
		
		if (orgFileName.lastIndexOf(".") != -1 && orgFileName.lastIndexOf(".") != 0 && !orgFileName.contains(".."))
			if (!orgFileName.substring(orgFileName.lastIndexOf(".") + 1).equalsIgnoreCase("csv")) {
				return new Response("Fail ! Only csv Format requiered", orgFileName, file.getContentType(),
						file.getSize());
			} else {
                   try {
					if(fileStorageService.storeFile(file, date)== null) {
					throw new FileStorageException("Could not store file,Already Exist ! " + orgFileName + ". Please try again!", null);
					}
					else {
						fileName = fileStorageService.storeFile(file, date);
					}
					//String status = sftpConfig.connectUploadSftpFiles(file);
					//if(status.equalsIgnoreCase("Success")){}
					ArrayList<CashbackFileDTO> list = fileStorageService.readCsvFile(file, date, "ADMIN",fileName);
					String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/getCashback/")
							.path(fileName).toUriString();
					return new Response("File has been uploaded successfully", fileName, file.getContentType(), file.getSize());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return new Response("failure", orgFileName, file.getContentType(), file.getSize());

				}
            }
		return null;
	}
	
	public boolean isThisDateValid(String dateToValidate, String dateFromat) {

		if (dateToValidate == null) {
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);

		try {
			if(dateToValidate.charAt(4)=='-' && dateToValidate.charAt(7)=='-'){
			// if not valid, it will throw ParseException
			Date date = sdf.parse(dateToValidate);
			System.out.println(date);
			}else {
				return false;
			}

		} catch (ParseException e) {

			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	
}
