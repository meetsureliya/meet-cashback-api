package com.onepay.controller;

import java.text.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.onepay.Response.Response;
import com.onepay.service.CommonMethodService;
import com.onepay.service.RejectCashBackFileService;

@RestController
public class RejectCashBackFileController {
	
	@Autowired
	private CommonMethodService commonMethodService;
	
	@Autowired
	private RejectCashBackFileService rejectCashBackFileService;

	@PostMapping("/RejectCashback")
	public Response RejectCashbackFile(@RequestParam("file") MultipartFile file) throws ParseException {
		boolean status = false;
		String orgFileName = file.getOriginalFilename();
		if(commonMethodService.findFile(orgFileName) != true ) {
			return new Response("Fail ! File Not exist!", orgFileName, file.getContentType(),
					file.getSize());
		}

         if (orgFileName.lastIndexOf(".") != -1 && orgFileName.lastIndexOf(".") != 0 && !orgFileName.contains(".."))
				if (!orgFileName.substring(orgFileName.lastIndexOf(".") + 1).equalsIgnoreCase("csv")) {
					return new Response("Fail ! Only csv Format requiered", orgFileName, file.getContentType(),
							file.getSize());
				} else {
	                   try {
						 status = rejectCashBackFileService.rejectCsvFileData(file);
						 if(status=true)
						 {
						   return new Response("File has been Reject successfully", orgFileName, file.getContentType(), file.getSize());
                         }
						 else{
							   return new Response("File has not Reject successfully", orgFileName, file.getContentType(), file.getSize());
                          }
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return new Response("failure", orgFileName, file.getContentType(), file.getSize());

					}
	            }
			return null;

	}


}
