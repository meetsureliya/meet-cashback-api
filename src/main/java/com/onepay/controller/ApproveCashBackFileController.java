package com.onepay.controller;

import java.text.ParseException;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.onepay.Response.Response;
import com.onepay.model.CashbackFileDTO;
import com.onepay.service.ApproveCashBackFileService;
import com.onepay.service.CommonMethodService;
import com.onepay.service.UploadCashbackFileService;

@RestController
public class ApproveCashBackFileController {
	
	@Autowired
	private CommonMethodService commonMethodService;
	
	@Autowired
	private ApproveCashBackFileService approveCashBackFileService;

	@PostMapping("/AproveCashback")
	public ApproveCashBackResponse AproveCashbackFile(@RequestParam("file") MultipartFile file) throws ParseException {

		String orgFileName = file.getOriginalFilename();
		if(commonMethodService.findFile(orgFileName) != true ) {
			return new ApproveCashBackResponse("Fail ! File Not exist!", orgFileName,  file.getContentType(),
					file.getSize(),null);
		}

         if (orgFileName.lastIndexOf(".") != -1 && orgFileName.lastIndexOf(".") != 0 && !orgFileName.contains(".."))
				if (!orgFileName.substring(orgFileName.lastIndexOf(".") + 1).equalsIgnoreCase("csv")) {
					return new ApproveCashBackResponse("Fail ! Only csv Format requiered", orgFileName,  file.getContentType(),
							file.getSize(),null);
				} else {
	                   try {
						 ArrayList<String> status = approveCashBackFileService.approveCsvFileData(file);
						 if(status!=null)
						 {
								return new ApproveCashBackResponse("File has been Approved successfully", orgFileName, file.getContentType(), file.getSize(),status);
	 
						 }else {
								return new ApproveCashBackResponse("File has not been Approved successfully", orgFileName, file.getContentType(), file.getSize(),status);
	 
						 }
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return new ApproveCashBackResponse("failure", orgFileName, file.getContentType(), file.getSize(),null);

					}
	            }
			return null;

	}


}
