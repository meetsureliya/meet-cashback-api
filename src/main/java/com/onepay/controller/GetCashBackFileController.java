package com.onepay.controller;

import java.io.FileNotFoundException;
import java.util.Map;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.onepay.model.CashbackFileDTO;
//import com.onepay.model.Response;
import com.onepay.Response.getCashBackResponse;
import com.onepay.service.GetCashBackFileService;
import com.onepay.service.UploadCashbackFileService;

@RestController
public class GetCashBackFileController {

	@Autowired
	private GetCashBackFileService getCashBackFileService;
	
	@Autowired
	private UploadCashbackFileService uploadCashbackFileService; 

	@PostMapping("/getCashbackByDate")
	public Map<String, ArrayList<getCashBackResponse>> getCahbackbyDate(@RequestParam("fromdate") String fromDate,
			@RequestParam("todate") String toDate) throws ParseException {

		Map<String, ArrayList<getCashBackResponse>> map = new HashMap<String, ArrayList<getCashBackResponse>>();

		if (isThisDateValid(fromDate, "yyyy-MM-dd") != true) {
			map.put("Fail ! fromDate dateFormat is not Correct, Kindly Use yyyy-MM-dd", null);
			return map;
		}
		if (isThisDateValid(toDate, "yyyy-MM-dd") != true) {
			map.put("Fail ! toDate dateFormat is not Correct, Kindly Use yyyy-MM-dd", null);
			return map;
		}
		try {

			Date fromdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").parse(fromDate + " 00:00:00.000000");
			Date todate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").parse(toDate + " 23:59:59.999999");

			if (fromDate.compareTo(toDate) > 0) {
				map.put("Fail ! from date must be less then to date", null);
				return map;
			}

			ArrayList<getCashBackResponse> response = getCashBackFileService.getcashbackData(fromdate, todate);

			if (response.isEmpty()) {
				map.put("Fail ! Data is not there", response);
				return map;
			} else {
				map.put("Success List", response);
				return map;
			}

		} catch (Exception e) {
			e.printStackTrace();
			map.put(e.getMessage(), null);
			return map;
		}

	}

	@PostMapping("/getCashbackByFile")
	public Map<String, ArrayList<getCashBackResponse>> getCahbackbyFile(@RequestParam("fileName") String fileName)
			throws ParseException {

		Map<String, ArrayList<getCashBackResponse>> map = new HashMap<String, ArrayList<getCashBackResponse>>();

		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0 && !fileName.contains(".."))
			if (!fileName.substring(fileName.lastIndexOf(".") + 1).equalsIgnoreCase("csv")) {
				map.put("Fail ! Only csv Format requiered", null);
				return map;
			}
		try {
			ArrayList<getCashBackResponse> response = getCashBackFileService.getcashbackData(fileName);

			if (response.isEmpty()) {
				map.put("Fail ! File is not there", response);
				return map;
			} else {
				map.put("Success List", response);
				return map;
			}

		} catch (Exception e) {
			e.printStackTrace();
			map.put(e.getMessage(), null);
			return map;
		}

	}

	@GetMapping("/downloadFile/{fileName:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request)
			throws FileNotFoundException {
		final Logger logger = LoggerFactory.getLogger(UploadCashBackFileController.class);

		// Load file as Resource
		Resource resource = uploadCashbackFileService.loadFileAsResource(fileName);

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			logger.info("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

	public boolean isThisDateValid(String dateToValidate, String dateFromat) {

		if (dateToValidate == null) {
			return false;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);

		try {

			// if not valid, it will throw ParseException
			Date date = sdf.parse(dateToValidate);
			System.out.println(date);

		} catch (ParseException e) {

			e.printStackTrace();
			return false;
		}

		return true;
	}

}
