package com.onepay.Response;

import java.util.ArrayList;

public class ApproveCashBackResponse {
	private String status;
	private String fileName;
     private String fileType;
    private long size;
    ArrayList<String> ApprovedAccount;

	
    public ApproveCashBackResponse(String status,String fileName,  String fileType, long size, ArrayList<String> ApprovedAccount ) {
        this.fileName = fileName;
        
        this.fileType = fileType;
        this.size = size;
        this.status = status;
        this.ApprovedAccount = ApprovedAccount;
        
    }
    
    public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public ArrayList<String> getApprovedAccount() {
		return ApprovedAccount;
	}
	public void setApprovedAccount(ArrayList<String> approvedAccount) {
		ApprovedAccount = approvedAccount;
	}



}
