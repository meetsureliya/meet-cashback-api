package com.onepay.Response;

import java.util.ArrayList;
import java.util.Date;

public class getCashBackResponse {
	private String status;
 	private Date cashbackDate;
    private String fileName;
    private String uploadedFileName;
    private String totalWallet;
    private String matchWallet;
    private String totalAmount;
    private String missMatchWallet;
    private Date fileUploadedDate;
    private String fileUploadedBy;
	public getCashBackResponse(String string , Date fromDate, String string2, String string3, String string4,
			String string5, String string6, String string7, Date toDate, String string8) {
		// TODO Auto-generated constructor stub
	}
	public getCashBackResponse() {
		// TODO Auto-generated constructor stub
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCashbackDate() {
		return cashbackDate;
	}
	public void setCashbackDate(Date cashbackDate) {
		this.cashbackDate = cashbackDate;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getUploadedFileName() {
		return uploadedFileName;
	}
	public void setUploadedFileName(String uploadedFileName) {
		this.uploadedFileName = uploadedFileName;
	}
	public String getTotalWallet() {
		return totalWallet;
	}
	public void setTotalWallet(String totalWallet) {
		this.totalWallet = totalWallet;
	}
	public String getMatchWallet() {
		return matchWallet;
	}
	public void setMatchWallet(String matchWallet) {
		this.matchWallet = matchWallet;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getMissMatchWallet() {
		return missMatchWallet;
	}
	public void setMissMatchWallet(String missMatchWallet) {
		this.missMatchWallet = missMatchWallet;
	}
	public Date getFileUploadedDate() {
		return fileUploadedDate;
	}
	public void setFileUploadedDate(Date fileUploadedDate) {
		this.fileUploadedDate = fileUploadedDate;
	}
	public String getFileUploadedBy() {
		return fileUploadedBy;
	}
	public void setFileUploadedBy(String fileUploadedBy) {
		this.fileUploadedBy = fileUploadedBy;
	}
	 
}
