package com.onepay.Response;

import java.util.ArrayList;

public class Response {
	private String status;
	private String fileName;
     private String fileType;
    private long size;
   
	public Response(String status,String fileName,  String fileType, long size ) {
        this.fileName = fileName;
        
        this.fileType = fileType;
        this.size = size;
        this.status = status;
        
    }
    
    public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


  
	
}
