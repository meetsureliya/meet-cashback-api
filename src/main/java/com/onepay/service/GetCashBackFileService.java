package com.onepay.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onepay.dao.CashBackDAO;
import com.onepay.model.CashbackFileDTO;
import com.onepay.Response.getCashBackResponse;

@Service
public class GetCashBackFileService {
	
		@Autowired
		private CashBackDAO cashBackDao;
		
	public ArrayList<getCashBackResponse> getcashbackData(Date fromdate, Date todate) {

		ArrayList<getCashBackResponse> cashbackresponseList = new ArrayList<getCashBackResponse>();
		ArrayList<CashbackFileDTO> cashbacklist = cashBackDao.getBetween(fromdate,todate);
		for (CashbackFileDTO cashbackList : cashbacklist) {
			getCashBackResponse getCashBackresponse = new getCashBackResponse();
			getCashBackresponse.setStatus("SUCCESS");
			getCashBackresponse.setCashbackDate(cashbackList.getProcess_datetime());
			getCashBackresponse.setFileName(cashbackList.getUploaded_file_name());
			getCashBackresponse.setFileUploadedBy(cashbackList.getUploaded_by());
			getCashBackresponse.setFileUploadedDate(cashbackList.getUpload_date_time() );
			getCashBackresponse.setMatchWallet(cashbackList.getValid_count());
			getCashBackresponse.setMissMatchWallet(cashbackList.getInvalid_count());
			getCashBackresponse.setTotalAmount(cashbackList.getTotal_amount() );
			getCashBackresponse.setTotalWallet(cashbackList.getTotal_count());
			getCashBackresponse.setUploadedFileName(cashbackList.getFile_name() );
			cashbackresponseList.add(getCashBackresponse);
		}
		return cashbackresponseList;
		
	}
	
	public ArrayList<getCashBackResponse> getcashbackData(String fileName) {

		ArrayList<getCashBackResponse> cashbackresponseList = new ArrayList<getCashBackResponse>();
		ArrayList<CashbackFileDTO> cashbacklist = cashBackDao.getByFile(fileName);
		for (CashbackFileDTO cashbackList : cashbacklist) {
			getCashBackResponse getCashBackresponse = new getCashBackResponse();
			getCashBackresponse.setStatus("SUCCESS");
			getCashBackresponse.setCashbackDate(cashbackList.getProcess_datetime());
			getCashBackresponse.setFileName(cashbackList.getUploaded_file_name());
			getCashBackresponse.setFileUploadedBy(cashbackList.getUploaded_by());
			getCashBackresponse.setFileUploadedDate(cashbackList.getUpload_date_time() );
			getCashBackresponse.setMatchWallet(cashbackList.getValid_count());
			getCashBackresponse.setMissMatchWallet(cashbackList.getInvalid_count());
			getCashBackresponse.setTotalAmount(cashbackList.getTotal_amount() );
			getCashBackresponse.setTotalWallet(cashbackList.getTotal_count());
			getCashBackresponse.setUploadedFileName(cashbackList.getFile_name() );
			cashbackresponseList.add(getCashBackresponse);
		}
		return cashbackresponseList;
		
	}

//	public ArrayList<getCashBackResponse> getcashbackData(Date fromdate, Date todate) {
//
//		ArrayList<getCashBackResponse> cashbackresponseList = new ArrayList<getCashBackResponse>();
//		ArrayList<WalletOpsTxnDTO> cashbacklist = cashBackDao.getBetween(fromdate,todate);
//		for (WalletOpsTxnDTO cashbackList : cashbacklist) {
//			getCashBackResponse getCashBackresponse = new getCashBackResponse();
//			getCashBackresponse.setStatus("SUCCESS");
//			getCashBackresponse.setCashbackDate(cashbackList.getCreated_on() );
//			getCashBackresponse.setFileName(cashbackList.getUploaded_file_name());
//			getCashBackresponse.setFileUploadedBy(cashbackList.getCreated_by() );
//			getCashBackresponse.setFileUploadedDate(cashbackList.getCreated_on() );
//			getCashBackresponse.setMatchWallet("");
//			getCashBackresponse.setMissMatchWallet("");
//			getCashBackresponse.setTotalAmount("");
//			getCashBackresponse.setTotalWallet("");
//			getCashBackresponse.setUploadedFileName(cashbackList.getUploaded_file_name());
//			cashbackresponseList.add(getCashBackresponse);
//		}
//		return cashbackresponseList;
//		
//	}
}
