package com.onepay.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.onepay.dao.CashBackDAO;
import com.onepay.model.CashbackFileDTO;
import com.onepay.model.WalletOpsTxnDTO;
import com.onepay.walletservice.dto.WalletInfoDTO;
import com.onepay.walletservice.dto.WalletTxnInfoDTO;
import com.onepay.walletservice.service.WalletTopupService;
import com.onepay.walletservice.service.WalletValidationService;

@Service
public class ApproveCashBackFileService {
	@Autowired
	private CashBackDAO cashBackDao;
	
	@Autowired
	 private CommonMethodService commonMethodService;

	@Autowired
	private WalletValidationService walletValidationService;
	@Autowired
	private WalletInfoDTO walletInfoDTO;
	@Autowired
	private WalletTopupService walletTopupService;
	@Autowired
	private WalletTxnInfoDTO walletTxnInfoDTO;

	@SuppressWarnings("unused")
	public ArrayList<String> approveCsvFileData(MultipartFile file) throws SQLException, Exception {
		CSVParser parser = null;
		try {
			CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(',');
			File convFile = commonMethodService.convertToFile(file);
			// initialize the CSVParser object
			 parser = new CSVParser(new FileReader(convFile), format);

			ArrayList<WalletOpsTxnDTO> walletOpsTxnlist = new ArrayList<WalletOpsTxnDTO>();
			ArrayList<String> proceesedlist = new ArrayList<String>();
 			int amt = 0,proceesCount=0;
			for (CSVRecord record : parser) {
				WalletOpsTxnDTO walletOpsTxnDTO = new WalletOpsTxnDTO();
				//String walletInfoDTO=null;
				walletInfoDTO = walletValidationService.validateVRNBasedWallet(record.get("Account_no"));
				if (walletInfoDTO  != null) {

					walletTxnInfoDTO.setWalletID(record.get("Account_no"));
					walletTxnInfoDTO.setTxnAmount(Long.valueOf(record.get("Amount")));
					walletTopupService.topupWallet(walletTxnInfoDTO); 

					walletOpsTxnDTO.setWallet_id(record.get("Account_no"));
					walletOpsTxnDTO.setTxn_amount(
							BigDecimal.valueOf(Double.valueOf(record.get("Amount")) * 100).toBigInteger());
					walletOpsTxnDTO.setPartner_ref_number(record.get("partnerRefNo"));
					walletOpsTxnDTO.setRemark(record.get("remark"));
					walletOpsTxnDTO.setStatus("PROCESSED");
					walletOpsTxnDTO.setUploaded_file_name(convFile.getPath());
					walletOpsTxnDTO.setCreated_by("ADMIN");
					walletOpsTxnDTO.setCreated_on(new java.util.Date());
					walletOpsTxnDTO.setModified_on(new java.util.Date());
					walletOpsTxnDTO.setTxn_date(new java.util.Date());
				} else {
					walletOpsTxnDTO.setStatus("INPROGRESS");
					walletOpsTxnDTO.setWallet_id(record.get("Account_no"));
					walletOpsTxnDTO.setTxn_amount(
							BigDecimal.valueOf(Double.valueOf(record.get("Amount")) * 100).toBigInteger());
					walletOpsTxnDTO.setPartner_ref_number(record.get("partnerRefNo"));
					walletOpsTxnDTO.setRemark(record.get("remark"));
					walletOpsTxnDTO.setUploaded_file_name(convFile.getPath());
					walletOpsTxnDTO.setCreated_by("ADMIN");
					walletOpsTxnDTO.setCreated_on(new java.util.Date());
					walletOpsTxnDTO.setModified_on(new java.util.Date());
					walletOpsTxnDTO.setTxn_date(new java.util.Date());
				}
				walletOpsTxnlist.add(walletOpsTxnDTO);
				if(walletOpsTxnDTO.getStatus().equalsIgnoreCase("PROCESSED")) {
					proceesCount= proceesCount +1 ;
					proceesedlist.add(record.get("Account_no"));
					
				}
			}
 			System.out.println(walletOpsTxnlist);
			// save File Data
			cashBackDao.saveAllWalletData(walletOpsTxnlist,proceesCount,walletOpsTxnlist.size(),file.getOriginalFilename());
			return proceesedlist;
		} catch (Exception e) {
			return null;
		}finally {
			// close the parser
			parser.close();
		}
	
	}

	
}
