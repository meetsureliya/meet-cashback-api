package com.onepay.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import com.onepay.dao.CashBackDAO;
import com.onepay.exception.FileStorageException;
import com.onepay.model.CashbackFileDTO;
import com.onepay.model.FileStorageProperties;
 import com.onepay.walletservice.dto.WalletInfoDTO;
 import com.onepay.walletservice.service.WalletValidationService;
 import com.onepay.walletservice.dto.WalletTxnInfoDTO;
 import com.onepay.walletservice.service.WalletTopupService; 
 
 
@Service
public class UploadCashbackFileService {

	private final Path fileStorageLocation;

	 @Autowired
	 private CashBackDAO cashBackDao;
	 
	  @Autowired
	 private CommonMethodService commonMethodService;
	 
	 @Autowired private WalletValidationService walletValidationService;
	 @Autowired private WalletInfoDTO walletInfoDTO;
	 @Autowired private WalletTopupService walletTopupService;
	 @Autowired private WalletTxnInfoDTO walletTxnInfoDTO; 

	@Autowired
	public UploadCashbackFileService(FileStorageProperties fileStorageProperties) {
		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();

		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.",
					ex);
		}
	}

	public String storeFile(MultipartFile file) {
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}

			// Copy file to the target location (Replacing existing file with the same name)
			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

			return fileName;
		} catch (IOException ex) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
		}
	}

	@SuppressWarnings("deprecation")
	public String storeFile(MultipartFile file, Date date) throws IOException {
		// Normalize file name
		File convFile = commonMethodService.convertToFile(file);
		
		int currentYear=date.getYear()+1900; 
		String newFileName = "cashback_".concat(Integer.toString(currentYear))
				.concat(Integer.toString(date.getMonth())).concat(Integer.toString(date.getDate()))
				.concat(Integer.toString(date.getHours())).concat(Integer.toString(date.getMinutes()))
				.concat(Integer.toString(date.getSeconds())).concat(".csv");
		File newFile = new File(newFileName);
		convFile.renameTo(newFile);
		FileChannel src = new FileInputStream(convFile).getChannel();
		FileChannel dest = new FileOutputStream(newFile).getChannel();
		dest.transferFrom(src, 0, src.size());  
		
		String fileName = StringUtils.cleanPath(newFile.getName());
		
		if(cashBackDao.findByFileName(newFileName)== false)
		{
		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
			}
			InputStream targetStream = new FileInputStream(newFile);
			
			// Copy file to the target location (Replacing existing file with the same name)
			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Files.copy(targetStream, targetLocation, StandardCopyOption.REPLACE_EXISTING);

			return newFileName;
		} catch (IOException ex) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
		}else
		{
              return null;
		}
		
	
		
	}

	
	public Resource loadFileAsResource(String fileName) throws FileNotFoundException {
		try {
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new FileNotFoundException("File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			throw new FileNotFoundException("File not found " + fileName);
		}
	}
	
	

	public ArrayList<CashbackFileDTO> readCsvFile(MultipartFile file, Date date, String userType,String fileName) throws SQLException, Exception {
		java.util.Date dt = new java.util.Date();
		
		if (date == null) { dt = new java.util.Date(); } else { dt = date; }
		int i=0,j=0,k=0;
		// Create the CSVFormat object
		CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(',');
		File convFile = commonMethodService.convertToFile(file);
		// initialize the CSVParser object
		CSVParser parser = new CSVParser(new FileReader(convFile), format);

		ArrayList<CashbackFileDTO> cashbacklist = new ArrayList<CashbackFileDTO>();
 		int amt=0;
		for (CSVRecord record : parser) {
			CashbackFileDTO cashbackFileDTO = new CashbackFileDTO();
 			 
			cashbackFileDTO.setTotal_amount(String.valueOf((Integer.valueOf(record.get("Amount"))*100)+amt));
            amt=(Integer.valueOf(record.get("Amount"))*100);
            cashbackFileDTO.setTotal_count(String.valueOf(++k));
			cashbackFileDTO.setFile_name(fileName);
			cashbackFileDTO.setUploaded_file_name(convFile.getName());
			cashbackFileDTO.setProcess_datetime(dt);
			cashbackFileDTO.setUpload_date_time(dt);
			cashbackFileDTO.setStatus("PENDING");
			 walletInfoDTO = walletValidationService.validateVRNBasedWallet(record.get("Account_no"));
            if(walletInfoDTO!=null) {
            	cashbackFileDTO.setValid_count(String.valueOf(++i));
            }else {
            	cashbackFileDTO.setInvalid_count(String.valueOf(++j));
            }
            cashbackFileDTO.setTotal_count(String.valueOf(++k)); 
			cashbackFileDTO.setUploaded_by(userType);
			cashbacklist.add(cashbackFileDTO);
		 
		}
		 
		// close the parser
		parser.close();
		System.out.println(cashbacklist);
		// save File Data
		 cashBackDao.saveAll(cashbacklist );
		return cashbacklist;

	}
	

	 
}

