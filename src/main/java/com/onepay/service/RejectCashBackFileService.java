package com.onepay.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.onepay.dao.CashBackDAO;

@Service
public class RejectCashBackFileService {
	
	@Autowired
	 private CashBackDAO cashBackDao;
	 
	 @Autowired
	 private CommonMethodService commonMethodService;

	public boolean rejectCsvFileData(MultipartFile file) {

		try {

			CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(',');
			File convFile = commonMethodService.convertToFile(file);
			// initialize the CSVParser object
			CSVParser parser = new CSVParser(new FileReader(convFile), format);
			ArrayList<String> walletIdList = new ArrayList<>();

			for (CSVRecord record : parser) {
				walletIdList.add(record.get("Account_no"));
			}
			// close the parser
			parser.close();
			System.out.println(walletIdList);
			// save File Data
			boolean status = cashBackDao.updateRejectStatus(walletIdList, file.getOriginalFilename());
			return status;
		} catch (Exception e) {
			return false;
		}
	}


}
