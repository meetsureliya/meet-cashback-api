package com.onepay.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onepay.dao.CashBackDAO;

@Service
public class CommonMethodService {
	
	@Autowired
	 private CashBackDAO cashBackDao;

	public boolean findByOrgFileName(String orgFileName) {

		if (cashBackDao.findByOrgFileName(orgFileName) == true) {
			return true;

		} else {
			return false;
		}

	}

	public boolean findFile(String orgFileName) {

		if (cashBackDao.findByFileName(orgFileName) == true) {
			return true;

		} else {
			return false;
		}

	}
	
	 

	public File convertToFile(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

}
