
package com.onepay.dao;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.cj.Query;
import com.onepay.model.CashbackFileDTO;
import com.onepay.model.WalletOpsTxnDTO;
import com.onepay.repository.CashBackRepository;
import com.onepay.repository.WalletOpsTxnRepositor;

@Repository
public class CashBackDAO {

	@Autowired
	private CashBackRepository cashbackRepository;

	@Autowired
	private WalletOpsTxnRepositor walletOpsTxnRepository;

	@Transactional
	public ArrayList<CashbackFileDTO> saveAll(ArrayList<CashbackFileDTO> fileDataList) {
		ArrayList<CashbackFileDTO> result = new ArrayList<CashbackFileDTO>();

		cashbackRepository.insertCashBack(fileDataList.get(fileDataList.size() - 1).getFile_name(),
				fileDataList.get(fileDataList.size() - 1).getUploaded_file_name(),
				fileDataList.get(fileDataList.size() - 1).getProcess_datetime(),
				fileDataList.get(fileDataList.size() - 1).getStatus(),
				fileDataList.get(fileDataList.size() - 1).getUpload_date_time(),
				fileDataList.get(fileDataList.size() - 1).getUploaded_by(),
				fileDataList.get(fileDataList.size() - 1).getModified_on(),
				fileDataList.get(fileDataList.size() - 1).getTotal_count(),
				fileDataList.get(fileDataList.size() - 1).getValid_count(),
				fileDataList.get(fileDataList.size() - 1).getInvalid_count(),
				fileDataList.get(fileDataList.size() - 1).getTotal_amount());
		for (CashbackFileDTO fileData : fileDataList) {
			result.add(cashbackRepository.save(fileData));
		}
		return result;
	}

	@Transactional
	public ArrayList<WalletOpsTxnDTO> saveAllWalletData(ArrayList<WalletOpsTxnDTO> walletOpsTxnlist, int proceesCount,int totalCount, String originalFilename) {
		ArrayList<WalletOpsTxnDTO> result = new ArrayList<WalletOpsTxnDTO>();
		if(proceesCount == totalCount) {
			cashbackRepository.updateRejectStatus("PROCESSED", originalFilename);
        }else {
			cashbackRepository.updateRejectStatus("INPROGRESS", originalFilename);
        }
        for (WalletOpsTxnDTO walletData : walletOpsTxnlist) {

			walletOpsTxnRepository.save(walletData);
		}
		return result;
	}

	@Transactional
	public ArrayList<CashbackFileDTO> getAllCashBack() {
		ArrayList<CashbackFileDTO> result = (ArrayList<CashbackFileDTO>) cashbackRepository.findAll();
		return result;
	}

	public ArrayList<CashbackFileDTO> getBetween(Date fromdate, Date todate) {

		ArrayList<CashbackFileDTO> result1 = (ArrayList<CashbackFileDTO>) cashbackRepository.findBeetweenDates(fromdate,
				todate);

		return result1;
	}

	public ArrayList<CashbackFileDTO> getByFile(String fileName) {

		ArrayList<CashbackFileDTO> result1 = (ArrayList<CashbackFileDTO>) cashbackRepository
				.findByOrgFileName(fileName);

		return result1;
	}

	public boolean findByOrgFileName(String orgFileName) {

		if (!cashbackRepository.findByOrgFileName(orgFileName).isEmpty()) {
			return true;
		} else {
			return false;
		}
		// TODO Auto-generated method stub

	}

	public boolean findByFileName(String file_name) {

		if (!cashbackRepository.findByFileName(file_name).isEmpty()) {
			return true;
		} else {
			return false;
		}
		// TODO Auto-generated method stub

	}

	public boolean updateRejectStatus(ArrayList<String> walletIdList, String originalFilename) {

		try {
			cashbackRepository.updateRejectStatus("REJECT", originalFilename);
			for (String walletId : walletIdList) {
				walletOpsTxnRepository.updateRejectStatusWallet("REJECT", walletId);
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
