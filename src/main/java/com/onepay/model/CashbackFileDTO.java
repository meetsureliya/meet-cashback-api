package com.onepay.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "cashback_file")
public class CashbackFileDTO {
	
	@Id
    private String file_name;
	private String uploaded_file_name;
	private java.util.Date process_datetime;
	private String status;
	private java.util.Date upload_date_time;
	private String uploaded_by;
	private java.util.Date modified_on;
	private String total_count;
	private String valid_count;
	private String invalid_count;
	private String total_amount;
	
	public java.util.Date getModified_on() {
		return modified_on;
	}
	public void setModified_on(java.util.Date modified_on) {
		this.modified_on = modified_on;
	}
	public String getTotal_count() {
		return total_count;
	}
	public void setTotal_count(String total_count) {
		this.total_count = total_count;
	}
	public String getValid_count() {
		return valid_count;
	}
	public void setValid_count(String valid_count) {
		this.valid_count = valid_count;
	}
	public String getInvalid_count() {
		return invalid_count;
	}
	public void setInvalid_count(String invalid_count) {
		this.invalid_count = invalid_count;
	}
	public String getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getUploaded_file_name() {
		return uploaded_file_name;
	}
	public void setUploaded_file_name(String uploaded_file_name) {
		this.uploaded_file_name = uploaded_file_name;
	}
	public java.util.Date getProcess_datetime() {
		return process_datetime;
	}
	public void setProcess_datetime(java.util.Date process_datetime) {
		this.process_datetime = process_datetime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public java.util.Date getUpload_date_time() {
		return upload_date_time;
	}
	public void setUpload_date_time(java.util.Date upload_date_time) {
		this.upload_date_time = upload_date_time;
	}
	public String getUploaded_by() {
		return uploaded_by;
	}
	public void setUploaded_by(String uploaded_by) {
		this.uploaded_by = uploaded_by;
	}
	
	
	
	}
