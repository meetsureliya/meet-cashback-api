package com.onepay.model;

import java.math.BigInteger;
import javax.persistence.*;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wallet_ops_txn")
public class WalletOpsTxnDTO {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int wallet_ops_txn_id ;
	
	private String wallet_id ;
	private BigInteger txn_amount;
	private String txn_type;
	private String paymode;
	@Temporal(TemporalType.TIMESTAMP)
	private Date txn_date;
	private String status;
	private String uploaded_file_name;
	private String partner_ref_number;
	private String created_by;
	@Temporal(TemporalType.TIMESTAMP)
	private Date created_on;
	private String modified_by;
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified_on;
	
	private String udf1;
	private String udf2;
	private String udf3;
	private String udf4;
	private String udf5;
	private String remark;
	
	public int getWallet_ops_txn_id() {
		return wallet_ops_txn_id;
	}
	public void setWallet_ops_txn_id(int wallet_ops_txn_id) {
		this.wallet_ops_txn_id = wallet_ops_txn_id;
	}
	public String getWallet_id() {
		return wallet_id;
	}
	public void setWallet_id(String wallet_id) {
		this.wallet_id = wallet_id;
	}
	public BigInteger getTxn_amount() {
		return txn_amount;
	}
	public void setTxn_amount(BigInteger txn_amount) {
		this.txn_amount = txn_amount;
	}
	public String getTxn_type() {
		return txn_type;
	}
	public void setTxn_type(String txn_type) {
		this.txn_type = txn_type;
	}
	public String getPaymode() {
		return paymode;
	}
	public void setPaymode(String paymode) {
		this.paymode = paymode;
	}
	public Date getTxn_date() {
		return txn_date;
	}
	public void setTxn_date(Date txn_date) {
		this.txn_date = txn_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUploaded_file_name() {
		return uploaded_file_name;
	}
	public void setUploaded_file_name(String uploaded_file_name) {
		this.uploaded_file_name = uploaded_file_name;
	}
	public String getPartner_ref_number() {
		return partner_ref_number;
	}
	public void setPartner_ref_number(String partner_ref_number) {
		this.partner_ref_number = partner_ref_number;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public Date getCreated_on() {
		return created_on;
	}
	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}
	public String getModified_by() {
		return modified_by;
	}
	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}
	public Date getModified_on() {
		return modified_on;
	}
	public void setModified_on(Date modified_on) {
		this.modified_on = modified_on;
	}
	public String getUdf1() {
		return udf1;
	}
	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}
	public String getUdf2() {
		return udf2;
	}
	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}
	public String getUdf3() {
		return udf3;
	}
	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}
	public String getUdf4() {
		return udf4;
	}
	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}
	public String getUdf5() {
		return udf5;
	}
	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
