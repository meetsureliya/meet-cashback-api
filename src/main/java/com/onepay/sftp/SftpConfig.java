package com.onepay.sftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

@Component
public class SftpConfig {
	private static final String LOCAL_DIRECTORY = "sftp-inboundnewnew";

	@Value("${sftp.hostName}")
	private String HOST_NAME;

	@Value("${sftp.userName}")
	private String USER_NAME;

	@Value("${sftp.password}")
	private String PASSWORD;

	@Value("${sftp.remoteDirectory}")
	private String REMOTE_DIRECTORY;

	@Value("${sftp.remoteDirectoryProcessed}")
	private String REMOTE_DIRECTORY_PROCESSED;

	@Value("${sftp.remoteDirectoryPending}")
	private String REMOTE_DIRECTORY_PENDING;

	ObjectMapper mapper = new ObjectMapper();

	public String connectUploadSftpFiles(MultipartFile file) throws IOException {

		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();

		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		// System.out.println("preparing the host information for sftp.");
		try {
			JSch jsch = new JSch();
			session = jsch.getSession(USER_NAME, HOST_NAME, 22);
			session.setPassword(PASSWORD);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			System.out.println("Host connected.");
			channel = session.openChannel("sftp");
			channel.connect();
			System.out.println("sftp channel opened and connected.");
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(REMOTE_DIRECTORY + "/" + REMOTE_DIRECTORY_PENDING);

			channelSftp.put(new FileInputStream(convFile), convFile.getName());
			return "Success";
		} catch (Exception ex) {
			System.out.println("Exception found while tranfer the response.");
			return "failure while uploadFile ";
		} finally {

			channelSftp.exit();
			System.out.println("sftp Channel exited.");
			channel.disconnect();
			System.out.println("sftp Channel disconnected.");
			session.disconnect();
			System.out.println("Host Session disconnected.");
		}
	}
}
